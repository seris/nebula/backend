<?php

require_once dirname(__DIR__, 2) . '/config/app.php';

if (file_exists(SYMFONY_DIR . '/var/cache/prod/App_KernelProdContainer.preload.php')) {
    require SYMFONY_DIR . '/var/cache/prod/App_KernelProdContainer.preload.php';
}
