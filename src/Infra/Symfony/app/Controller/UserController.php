<?php

namespace App\Controller;

use App\Middleware\User\RegisterUserRequest;
use Domain\UseCases\User\Register\RegisterUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    #[Route('/users', name: 'user_index')]
    public function userIndex(
        RegisterUserRequest $request,
        RegisterUser $registerUser
    ) {
    }
}
