<?php

namespace Domain\UseCases\User\Register;

use Domain\Entities\User\UserEntity;
use Domain\UseCases\User\Register\Interfaces\IRegisterPresenter;

class RegisterPresenter implements IRegisterPresenter
{
    public function __construct(
        private ?array $violations = [],
        private ?UserEntity $userEntity = null
    ) {
    }

    public function present(RegisterUserInput $userViewModel): void
    {
        $this->userEntity = $userViewModel->getUser();
        $this->violations = $userViewModel->getViolations();
    }

    public function getUser(): ?UserEntity
    {
        return $this->userEntity;
    }

    public function getViolations(): ?array
    {
        return $this->violations;
    }
}
