<?php

namespace Domain\UseCases\User\Register;

use Domain\Interfaces\Repository\IReadRepository;
use Domain\Interfaces\Repository\IWriteRepository;
use Domain\UseCases\User\Register\Exceptions\UserAlreadyExistsException;
use Domain\UseCases\User\Register\Interfaces\IRegisterPresenter;
use Domain\UseCases\User\Register\Interfaces\IRegisterUser;

class RegisterUser implements IRegisterUser
{
    public function __construct(
        private readonly IWriteRepository $writeRepository,
        private readonly IReadRepository $readRepository,
        private readonly IRegisterPresenter $presenter
    ) {
    }

    /**
     * @throws UserAlreadyExistsException
     */
    public function execute(RegisterUserInput $userInput): void
    {
        if ($this->userIsAlreadyRegistered($userInput)) {
            throw new UserAlreadyExistsException();
        }

        $this->writeRepository->save($userInput);
        $this->presenter->present($userInput);
    }

    private function userIsAlreadyRegistered(RegisterUserInput $userInput): bool
    {
        $userFound = $this->readRepository->find(['liEmail' => $userInput->liEmail]);
        return count($userFound) > 0;
    }
}
