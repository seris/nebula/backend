<?php

namespace Domain\UseCases\User\Register\Interfaces;

use Domain\UseCases\User\Register\RegisterUserInput;

interface IRegisterUser
{
    public function execute(RegisterUserInput $userInput): void;
}
