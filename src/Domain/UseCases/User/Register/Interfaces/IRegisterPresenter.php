<?php

namespace Domain\UseCases\User\Register\Interfaces;

use Domain\UseCases\User\Register\RegisterUserInput;

interface IRegisterPresenter
{
    public function present(RegisterUserInput $userViewModel);
}
