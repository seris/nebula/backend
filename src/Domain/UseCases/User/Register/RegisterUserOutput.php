<?php

namespace Domain\UseCases\User\Register;

use Domain\Entities\User\UserEntity;
use Domain\Interfaces\IUseCaseOutput;

readonly class RegisterUserOutput implements IUseCaseOutput
{
    public function __construct(
        private ?array $violations = null,
        private ?UserEntity $userEntity = null
    ) {
    }

    public function getViolations(): ?array
    {
        return $this->violations;
    }

    public function getUser(): ?UserEntity
    {
        return $this->userEntity;
    }
}
