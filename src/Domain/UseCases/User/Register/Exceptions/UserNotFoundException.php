<?php

namespace Domain\UseCases\User\Register\Exceptions;

class UserNotFoundException extends \Exception
{
}
