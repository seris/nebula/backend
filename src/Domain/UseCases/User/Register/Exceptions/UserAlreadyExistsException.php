<?php

namespace Domain\UseCases\User\Register\Exceptions;

class UserAlreadyExistsException extends \Exception
{
    protected $message = 'User already exists.';
    protected $code = 400;
}
