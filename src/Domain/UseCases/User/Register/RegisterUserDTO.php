<?php

namespace Domain\UseCases\User\Register;

use Domain\Interfaces\IUseCaseInput;

class RegisterUserDTO implements IUseCaseInput
{
    public function __construct(
        public readonly string $liUser,
        public readonly string $liLogin,
        public readonly string $liEmail,
        public readonly string $idUserAuth,
        public readonly ?string $liPhoto = null,
        public readonly ?string $idMatricule = null
    ) {
    }
}
