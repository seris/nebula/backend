<?php

namespace Domain\UseCases\User\Register;

final class RegisterUserInput
{
    public function __construct(
        public readonly string $idUser,
        public readonly string $liUser,
        public readonly string $liLogin,
        public readonly string $liEmail,
        public readonly string $idUserAuth,
        public readonly ?string $liPhoto = null,
        public readonly ?string $idMatricule = null
    ) {
    }

    public function getIdUser(): string
    {
        return $this->idUser;
    }
}
