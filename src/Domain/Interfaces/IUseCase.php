<?php

namespace Domain\Interfaces;

/**
 * @template T
 */
interface IUseCase
{
    public function execute(IUseCaseInput $input): ?IUseCaseOutput;
}
