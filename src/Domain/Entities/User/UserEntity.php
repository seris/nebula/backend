<?php

namespace Domain\Entities\User;

final readonly class UserEntity
{
    public function __construct(
        private string $idUser,
        private string $liUser,
        private string $liLogin,
        private string $liEmail,
        private string $idUserAuth,
        private ?string $liPhoto = null,
        private ?string $idMatricule = null,
        private ?string $jsAccess = null
    ) {
    }

    public function getIdUser(): string
    {
        return $this->idUser;
    }

    public function getLiUser(): string
    {
        return $this->liUser;
    }

    public function getLiLogin(): string
    {
        return $this->liLogin;
    }

    public function getLiEmail(): string
    {
        return $this->liEmail;
    }

    public function getLiPhoto(): ?string
    {
        return $this->liPhoto;
    }

    public function getIdMatricule(): ?string
    {
        return $this->idMatricule;
    }

    public function getIdUserAuth(): ?string
    {
        return $this->idUserAuth;
    }

    public function getJsAccess(): ?string
    {
        return $this->jsAccess;
    }
}
