#!/bin/sh

alias sf="php bin/console"
alias sft="php bin/phpunit $1 --testdox"
alias sfc="sf cache:clear"
alias cpr="composer"

export LANG="fr_FR.utf8"
